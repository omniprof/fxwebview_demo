package com.kenfogel.fxwebview_demo.view;

import javafx.fxml.FXML;
import javafx.scene.web.WebView;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class FishFXWebViewController {

    private final static Logger LOG = LoggerFactory.getLogger(FishFXWebViewController.class);

    @FXML
    private WebView fishFXWebView;

    /**
     * Initializes the controller class. This method is automatically called
     * after the fxml file has been loaded. Not much to do here.
     */
    @FXML
    private void initialize() {
        final String html = "example.html";
        final java.net.URI uri = java.nio.file.Paths.get(html).toAbsolutePath().toUri();
        LOG.info("uri= " + uri.toString());

        // create WebView with specified local content
        fishFXWebView.getEngine().load(uri.toString());
        //fishFXWebView.getEngine().load("https://www.omnijava.com");
    }
}
