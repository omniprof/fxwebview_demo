package com.kenfogel.fxwebview_demo;

import java.io.IOException;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

//import com.kenfogel.fishfxwebview.view.FishFXWebViewController;
public class MainApp extends Application {

    private final static Logger LOG = LoggerFactory.getLogger(MainApp.class);

    private Stage primaryStage;
    private AnchorPane fishFXWebViewLayout;

    @Override
    public void start(Stage primaryStage) throws Exception {
        this.primaryStage = primaryStage;
        this.primaryStage.setTitle("Fish Web View");

        // Set the application icon using getResourceAsStream.
        this.primaryStage.getIcons().add(
                new Image(MainApp.class
                        .getResourceAsStream("/images/bluefish_icon.png")));

        initRootLayout();
        primaryStage.show();
    }

    /**
     * Load the layout and controller.
     */
    public void initRootLayout() {
        try {
            // Load root layout from fxml file.
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MainApp.class
                    .getResource("/fxml/FishFXWebViewLayout.fxml"));
            fishFXWebViewLayout = (AnchorPane) loader.load();

            // Show the scene containing the root layout.
            Scene scene = new Scene(fishFXWebViewLayout);
            primaryStage.setScene(scene);

            // If you need to send messages to the controller you can retrieve the reference to it.
            //FishFXWebViewController controller = loader.getController();
        } catch (IOException e) {
            LOG.error("Error displaying html editor", e);
        }
    }

    /**
     * Where it all begins
     *
     * @param args
     */
    public static void main(String[] args) {
        launch(args);
    }
}
